﻿/*
 * C# / XNA  port of Bullet (c) 2011 Mark Neale <xexuxjy@hotmail.com>
 *
 * Bullet Continuous Collision Detection and Physics Library
 * Copyright (c) 2003-2008 Erwin Coumans  http://www.bulletphysics.com/
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from
 * the use of this software.
 * 
 * Permission is granted to anyone to use this software for any purpose, 
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 * 
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

using System;
using BulletXNA.LinearMath;

namespace BulletXNA.BulletCollision
{
    public class SphereBoxCollisionAlgorithm : ActivatingCollisionAlgorithm
    {
        public SphereBoxCollisionAlgorithm() { } // for pool
        public SphereBoxCollisionAlgorithm(PersistentManifold mf, CollisionAlgorithmConstructionInfo ci, CollisionObject col0, CollisionObject col1, bool isSwapped)
            : base(ci, col0, col1)
        {
            m_isSwapped = isSwapped;
            m_ownManifold = false;
            m_manifoldPtr = mf;
            CollisionObject sphereObj = m_isSwapped ? col1 : col0;
            CollisionObject boxObj = m_isSwapped ? col0 : col1;

            if (m_manifoldPtr == null && m_dispatcher.NeedsCollision(sphereObj, boxObj))
            {
                m_manifoldPtr = m_dispatcher.GetNewManifold(sphereObj, boxObj);
                m_ownManifold = true;
            }
        }


        public void Initialize(PersistentManifold mf, CollisionAlgorithmConstructionInfo ci, CollisionObject col0, CollisionObject col1, bool isSwapped)
        {
            base.Initialize(ci, col0, col1);
            m_isSwapped = isSwapped;
            m_ownManifold = false;
            m_manifoldPtr = mf;

            CollisionObject sphereObj = m_isSwapped ? col1 : col0;
            CollisionObject boxObj = m_isSwapped ? col0 : col1;

            if (m_manifoldPtr == null && m_dispatcher.NeedsCollision(sphereObj, boxObj))
            {
                m_manifoldPtr = m_dispatcher.GetNewManifold(sphereObj, boxObj);
                m_ownManifold = true;
            }
        }

        public override void Cleanup()
        {
            if (m_ownManifold)
            {
                if (m_manifoldPtr != null)
                {
                    m_dispatcher.ReleaseManifold(m_manifoldPtr);
                    m_manifoldPtr = null;
                }
                m_ownManifold = false;
            }
            m_ownManifold = false;
            BulletGlobals.SphereBoxCollisionAlgorithmPool.Free(this);
        }

        public override void ProcessCollision(CollisionObject body0, CollisionObject body1, DispatcherInfo dispatchInfo, ManifoldResult resultOut)
        {
            //(void)dispatchInfo;
            //(void)resultOut;
            if (m_manifoldPtr == null)
            {
                return;
            }

            CollisionObject sphereObj = m_isSwapped ? body1 : body0;
            CollisionObject boxObj = m_isSwapped ? body0 : body1;

            Vector3 normalOnSurfaceB = new Vector3();
            Vector3 pOnBox = new Vector3(), pOnSphere = new Vector3();
            Vector3 sphereCenter = sphereObj.GetWorldTransform().Translation;
            SphereShape sphere0 = sphereObj.CollisionShape as SphereShape;
            float radius = sphere0.Radius;

            resultOut.SetPersistentManifold(m_manifoldPtr);

            float dist = GetSphereDistance(ref boxObj, ref pOnBox, ref pOnSphere, sphereCenter, radius);
            if(dist < MathUtil.SIMD_EPSILON)
            {
                normalOnSurfaceB = (pOnBox - pOnSphere).Normalized();
                resultOut.AddContactPoint(normalOnSurfaceB, pOnBox,dist);
            }

            if (m_ownManifold)
            {
                if (m_manifoldPtr.GetNumContacts() > 0)
                {
                    resultOut.RefreshContactPoints();
                }
            }
        }

        public override float CalculateTimeOfImpact(CollisionObject body0, CollisionObject body1, DispatcherInfo dispatchInfo, ManifoldResult resultOut)
        {
            return 1f;
        }

        public override void GetAllContactManifolds(ObjectArray<PersistentManifold> manifoldArray)
        {
            if (m_manifoldPtr != null && m_ownManifold)
            {
                manifoldArray.Add(m_manifoldPtr);
            }
        }

        public float GetSphereDistance(ref CollisionObject boxObj, ref Vector3 pointOnBox, ref Vector3 pointOnSphere, Vector3 sphereCenter, float fRadius)
        {
            float margins;
            Vector3[] bounds = new Vector3[2];

            BoxShape boxShape = boxObj.CollisionShape as BoxShape;

            bounds[0] = -boxShape.GetHalfExtentsWithoutMargin();
            bounds[1] = boxShape.GetHalfExtentsWithoutMargin();

            margins = boxShape.Margin;
            Vector3 boxHalfExtent = boxShape.GetHalfExtentsWithoutMargin();
            float boxMargin = boxShape.Margin;

            Matrix m44T = boxObj.GetWorldTransform();

            Vector3[] boundsVec = new Vector3[2];
            float fPenetration;

            boundsVec[0] = bounds[0];
            boundsVec[1] = bounds[1];

            Vector3 marginsVec = new Vector3(margins, margins, margins);

            bounds[0] += marginsVec;
            bounds[1] -= marginsVec;

            Vector3 tmp, prel, normal, v3P;
            Vector3[] n = new Vector3[6];
            float fSep = 10000000.0f, fSepThis;

            n[0] = new Vector3(-1, 0, 0);
            n[1] = new Vector3(0,-1,0);
            n[2] = new Vector3(0,0,-1);
            n[3] = new Vector3(1,0,0);
            n[4] = new Vector3(0,1,0);
            n[5] = new Vector3(0,0,1);

            prel = m44T.InvXform(sphereCenter);

            bool bFound = false;
            v3P = prel;

            for (int i = 0; i < 6; i++)
            {
                int j = i < 3 ? 0 : 1;
                if((fSepThis = ((v3P-bounds[j]).Dot(n[i]))) > 0.0f)
                {
                    v3P = v3P - n[i]*fSepThis;
                    bFound = true;
                }
            }

            if(bFound)
            {
                bounds[0] = boundsVec[0];
                bounds[1] = boundsVec[1];

                normal = (prel - v3P).Normalized();
                pointOnBox = v3P + normal*margins;
                pointOnSphere = prel - normal*fRadius;

                if(((pointOnSphere-pointOnBox).Dot(normal)) > 0.0f)
                {
                    return 1.0f;
                }

                tmp = m44T*pointOnSphere;
                pointOnSphere = tmp;
                float fSeps2 = (pointOnBox - pointOnSphere).LengthSquared();

                if(fSeps2 > MathUtil.SIMD_EPSILON)
                {
                    fSep = (float)-Math.Sqrt(fSeps2);
                    normal = (pointOnBox - pointOnSphere);
                    normal *= 1/fSep;
                }
                return fSep;
            }

            fPenetration = GetSpherePenetration(ref boxObj, ref pointOnBox, ref pointOnSphere, sphereCenter, fRadius, bounds[0], bounds[1]);
            bounds[0] = boundsVec[0];
            bounds[1] = boundsVec[1];

            if(fPenetration <= 0.0f)
            {
                return (fPenetration - margins);
            }
            else
            {
                return 1.0f;
            }
        }




        public float GetSpherePenetration(ref CollisionObject boxObj, ref Vector3 pointOnBox, ref Vector3 pointOnSphere, Vector3 sphereCenter, float fRadius, Vector3 aabbMin, Vector3 aabbMax)
        {
            Vector3[] bounds = new Vector3[2];
            bounds[0] = aabbMin;
            bounds[1] = aabbMax;

            Vector3 p0, tmp, prel, normal;
            Vector3[] n = new Vector3[6];
            float fSep = -10000000.0f, fSepThis;

            p0 = new Vector3(0,0,0);
            normal = new Vector3(0,0,0);

            n[0] = new Vector3(-1, 0, 0);
            n[1] = new Vector3(0, -1, 0);
            n[2] = new Vector3(0, 0, -1);
            n[3] = new Vector3(1, 0, 0);
            n[4] = new Vector3(0, 1, 0);
            n[5] = new Vector3(0, 0, 1);

            Matrix m44T = boxObj.GetWorldTransform();
            prel = m44T.InvXform(sphereCenter);

            for (int i = 0; i < 6; i++)
            {
                int j = i < 3 ? 0 : 1;
                if((fSepThis = ((prel-bounds[j]).Dot(n[i]))-fRadius) > 0.0f)
                {
                    return 1.0f;
                }
                if(fSepThis > fSep)
                {
                    p0 = bounds[j];
                    normal = n[i];
                    fSep = fSepThis;
                }
            }

            pointOnBox = prel - normal*(normal.Dot((prel - p0)));
            pointOnSphere = pointOnBox + normal*fSep;

            tmp = m44T*pointOnBox;
            pointOnBox = tmp;
            tmp = m44T*pointOnSphere;
            pointOnSphere = tmp;
            normal = (pointOnBox - pointOnSphere).Normalized();

            return fSep;
        }

        private bool m_ownManifold;
        private PersistentManifold m_manifoldPtr;
        private bool m_isSwapped;
    }


    public class SphereBoxCreateFunc : CollisionAlgorithmCreateFunc
    {
        public override CollisionAlgorithm CreateCollisionAlgorithm(CollisionAlgorithmConstructionInfo ci, CollisionObject body0, CollisionObject body1)
        {
            SphereBoxCollisionAlgorithm algo = BulletGlobals.SphereBoxCollisionAlgorithmPool.Get();
            algo.Initialize(null, ci, body0, body1, false);
            return algo;
        }
    }

    public class SwappedSphereBoxCreateFunc : CollisionAlgorithmCreateFunc
    {
        public override CollisionAlgorithm CreateCollisionAlgorithm(CollisionAlgorithmConstructionInfo ci, CollisionObject body0, CollisionObject body1)
        {
            SphereBoxCollisionAlgorithm algo = BulletGlobals.SphereBoxCollisionAlgorithmPool.Get();
            algo.Initialize(null, ci, body0, body1, true);
            return algo;
        }
    }



}
