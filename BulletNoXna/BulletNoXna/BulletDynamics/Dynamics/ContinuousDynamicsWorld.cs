﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BulletXNA;
using BulletXNA.BulletCollision;
using BulletXNA.BulletDynamics;
using BulletXNA.LinearMath;

namespace BulletNoXna.BulletDynamics.Dynamics
{
    public class ContinuousDynamicsWorld : DiscreteDynamicsWorld
    {
        public ContinuousDynamicsWorld(IDispatcher dispatcher, IBroadphaseInterface pairCache, IConstraintSolver constraintSolver, ICollisionConfiguration collisionConfiguration)
            : base(dispatcher, pairCache, constraintSolver, collisionConfiguration)
        {
            
        }

        protected override void InternalSingleStepSimulation(float timeStep)
        {

            StartProfiling(timeStep);

            if (m_internalPreTickCallback != null)
            {
                m_internalPreTickCallback.InternalTickCallback(this, timeStep);
            }


            ///update aabbs information
            UpdateAabbs();
            //static int frame=0;
            //	printf("frame %d\n",frame++);

            ///apply gravity, predict motion
            PredictUnconstraintMotion(timeStep);

            DispatcherInfo dispatchInfo = DispatchInfo;

            dispatchInfo.SetTimeStep(timeStep);
            dispatchInfo.SetStepCount(0);
            dispatchInfo.m_debugDraw = DebugDrawer;

            ///perform collision detection
            PerformDiscreteCollisionDetection();

            CalculateSimulationIslands();


            GetSolverInfo().m_timeStep = timeStep;



            ///solve contact and other joint constraints
            SolveConstraints(GetSolverInfo());

            ///CallbackTriggers();
            CalculateTimeOfImpacts(timeStep);

            float toi = dispatchInfo.GetTimeOfImpact();
            //	if (toi < 1.f)
            //		printf("toi = %f\n",toi);
            if (toi < 0.0f)
                if (BulletGlobals.g_streamWriter != null && BulletGlobals.debugDiscreteDynamicsWorld)
                {
                    BulletGlobals.g_streamWriter.WriteLine("InternalSingleStepSimulation [{0}]", toi);
                }

            ///integrate transforms
            IntegrateTransforms(timeStep*toi);

            ///update vehicle simulation
            UpdateActions(timeStep);

            UpdateActivationState(timeStep);

            if (m_internalTickCallback != null)
            {
                m_internalTickCallback.InternalTickCallback(this, timeStep);
            }
        }

        public virtual void CalculateTimeOfImpacts(float timeStep)
        {
            //these should be 'temporal' aabbs!
            UpdateTemporalAabbs(timeStep);

            //'toi' is the global smallest time of impact. However, we just calculate the time of impact for each object individually.
            //so we handle the case moving versus static properly, and we cheat for moving versus moving
            float toi = 1.0f;


            DispatcherInfo dispatchInfo = DispatchInfo;
            dispatchInfo.SetTimeStep(timeStep);
            dispatchInfo.SetTimeOfImpact(1.0f);
            dispatchInfo.SetStepCount(0);
            dispatchInfo.SetDispatchFunc(DispatchFunc.DISPATCH_DISCRETE);

            //calculate time of impact for overlapping pairs
            IDispatcher dispatcher = GetDispatcher();
            if (dispatcher != null)
                dispatcher.DispatchAllCollisionPairs(m_broadphasePairCache.GetOverlappingPairCache(), dispatchInfo,
                                                     m_dispatcher1);

            toi = dispatchInfo.GetTimeOfImpact();

            dispatchInfo.SetDispatchFunc(DispatchFunc.DISPATCH_DISCRETE);
        }

        public override DynamicsWorldType GetWorldType()
        {
            return DynamicsWorldType.BT_CONTINUOUS_DYNAMICS_WORLD;
        }

        private void UpdateTemporalAabbs(float timeStep)
        {
            Vector3 temporalAabbMin, temporalAabbMax;

            for (int i = 0; i < m_collisionObjects.Count; i++)
            {
                CollisionObject colObj = m_collisionObjects[i];

                RigidBody body = RigidBody.Upcast(colObj);
                if (body != null)
                {
                    body.CollisionShape.GetAabb(m_collisionObjects[i].GetWorldTransform(), out temporalAabbMin,
                                                out temporalAabbMax);
                    Vector3 linvel = body.LinearVelocity;

                    //make the AABB temporal
                    float temporalAabbMaxx = temporalAabbMax.X;
                    float temporalAabbMaxy = temporalAabbMax.Y;
                    float temporalAabbMaxz = temporalAabbMax.Z;
                    float temporalAabbMinx = temporalAabbMin.X;
                    float temporalAabbMiny = temporalAabbMin.Y;
                    float temporalAabbMinz = temporalAabbMin.Z;

                    // add linear motion
                    Vector3 linMotion = linvel*timeStep;

                    if (linMotion.X > 0.0f)
                        temporalAabbMaxx += linMotion.X;
                    else
                        temporalAabbMinx += linMotion.X;
                    if (linMotion.Y > 0.0f)
                        temporalAabbMaxy += linMotion.Y;
                    else
                        temporalAabbMiny += linMotion.Y;
                    if (linMotion.Z > 0.0f)
                        temporalAabbMaxz += linMotion.Z;
                    else
                        temporalAabbMinz += linMotion.Z;

                    //add conservative angular motion
                    float angularMotion = 0; // = angvel.length() * GetAngularMotionDisc() * timeStep;
                    Vector3 angularMotion3d = new Vector3(angularMotion, angularMotion, angularMotion);
                    temporalAabbMin = new Vector3(temporalAabbMinx, temporalAabbMiny, temporalAabbMinz);
                    temporalAabbMax = new Vector3(temporalAabbMaxx, temporalAabbMaxy, temporalAabbMaxz);

                    temporalAabbMin -= angularMotion3d;
                    temporalAabbMax += angularMotion3d;

                    m_broadphasePairCache.SetAabb(body.BroadphaseHandle, ref temporalAabbMin, ref temporalAabbMax,
                                                  m_dispatcher1);
                }
            }

            //update aabb (of all moved objects)

            m_broadphasePairCache.CalculateOverlappingPairs(m_dispatcher1);
        }

    }
}
